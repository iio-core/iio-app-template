# Deploy from development host
# ----------------------------

#!/bin/sh

if [ -z $1 ]
then
  echo "You need to provide a hostname: ./deploy.sh <hostname>"
  exit 1
fi

echo "Deploying on $1..."
echo "Prepare docker-compose.yml..."
cat template.docker-compose.yml | sed "s/iiozero/$1/g" > docker-compose.yml

echo "Copy compose file to remote deploy endpoint on iiozero..."
scp docker-compose.yml ignitial@iiozero:/opt/dep

echo "Pull images on $1..."
ssh ignitial@iiozero "ssh ignitial@$1 'docker pull registry.ignitial.io/ignitial/iioat'"
ssh ignitial@iiozero "ssh ignitial@$1 'docker pull registry.ignitial.io/ignitial/dlake-iioat'"

echo "Deploy remote from iiozero to $1..."
ssh ignitial@iiozero 'bash -s' < remote/stack_deploy.sh

echo "...DONE."
