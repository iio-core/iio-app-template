import MyView from '../views/MyView.vue'

export default [
  {
    path: '/myview',
    component: MyView,
    beforeEnter: (to, from, next) => {
      let token = localStorage.getItem('token')
      if (token && token !== 'null') {
        next()
      } else {
        next({ path: '/login' })
      }
    }
  }
]
