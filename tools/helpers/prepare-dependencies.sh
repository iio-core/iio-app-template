#!/bin/sh

echo "Create mandatory folders..."
mkdir -p ~/iio-data
mkdir -p ~/iio-data/mongo
mkdir -p ~/iio-data/minio
mkdir -p ~/iio-data/minio/config
mkdir -p ~/iio-data/minio/data

echo "Copy Minio configuration..."
cp tools/helpers/minio/config.json ~/iio-data/minio/config

echo "Prepare docker-compose.yml..."
APP_NAME=$(sed -n 's/.*"name": "\(.*\)",/\1/p' package.json)
cat tools/helpers/template.docker-compose.yml | sed "s/_user_/$USER/g" | sed "s/_appname_/$APP_NAME/g" > docker-compose.yml

echo "Prepare dev start bash file..."
cp tools/helpers/_start.sh dev_start.sh
