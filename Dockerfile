FROM node:10-alpine

RUN mkdir -p /opt && mkdir -p /opt/iioat

ADD . /opt/iioat

WORKDIR /opt/iioat

RUN npm install && npm run build:prod

CMD ["node", "./server/index.js"]
