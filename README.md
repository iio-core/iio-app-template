# Ignitial.IO app template

## Getting started  

### Local run ( = running on a developper machine)


#### Pre-requisites

- Declare your swarm master IP (or one of them) as iiozero in /etc/hosts (could be any other name, but you need then to 
update iiozero with new one every where in the deployment files).

_Note_: this will be changed in the future and replaced with an enf variable: $IIO_SWARM_MASTER

- Be logged into registry.ignitial.io Docker private registry (or update image tags for targetting your one registry or 
a public one like Docker hub).

- Make dlake image available (see [dlake IIO service](https://gitlab.com/iio-core/dlake-service/blob/master/README.md)).

- Add your SSH public key to the authorized keys on the Swarm Master

#### Prepare  

Four steps:
- build and publish on registry.ignitial.io dedicated dlake image (including specific datums)
- build and publish app image on registry.ignitial.io
- prepare dependencies (data folder, minio/minio config, docker-compose.yml specific file)
- install app specific node dependencies 

```bash
npm run publish:dlake:docker
npm run publish:docker

./tools/helpers/prepare-dependencies.sh

npm i
```

#### Populate

Insert minimal data into DB in order to be able to connect and operate.

```bash
./populate-mongo.sh
```

#### Run

```bash
./dev_start.sh
```

### Remote deploy (e.g. production datacenter)

#### Populate

When using MongoDB Atlas, you'd better populate first (in order to avoid restarting dlake service). In order to do
so, execute following script:

```bash
./populate-atlas.sh
```

You can configure URI (then Atlas replica set) and Atlas credentials using ENV variables as defined in the shell 
script. Atlas password can be defined as an export:

```bash
export MONGODB_PASSWORD=...
```

#### Deploy

Credentials (S3, Mongo, SMTP server, etc.) have to be defined as [Docker secrets](https://docs.docker.com/engine/swarm/secrets/).

```bash
cd deploy
./deploy.sh <hostname>
```

#### Clean up

```bash
cd deploy
./remove.sh
```
