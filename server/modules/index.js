const fs = require('fs')
const path = require('path')

const pino = require('@ignitial/iio-app/server/utils').pino.child({ origin: 'app-modules' })

for (let f of fs.readdirSync(__dirname)) {
  if (path.basename(f, '.js') !== 'index' && path.extname(f) !== '.map') {
    pino.info('registering module [%s] from folder [%s]', f, __dirname)

    let m = require('./' + f)

    exports[m._name] = m._main
  }
}
