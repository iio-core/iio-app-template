const Module = require('@ignitial/iio-app/server/modules/core').Module

exports._name = 'mymodule'

class MyModule extends Module {
  constructor(options) {
    super({
      name: 'mymodule',
      ...options
    })
  }
}

exports._main = MyModule
