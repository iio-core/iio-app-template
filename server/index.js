const Server = require('@ignitial/iio-app').Server
const config = require('./config')
const serverModules = require('./modules')
const _unifiedServices = require('./services')

function initialize(appManager) {
  // load & instantiate additional services modules
  for (let _name in serverModules) {
    if (_name) {
      appManager._useModule(_name, serverModules[_name])
    }
  }

  // unified services
  appManager.ws.on('client', clientId => {
    // loads additional unified services declared in app unified index file
    try {
      for (let s in _unifiedServices) {
        appManager.ws.addService(s,
          _unifiedServices[s],
          appManager._config.unified.options[s],
          clientId)
      }
    } catch(err) {
      console.log(err)
    }
  })
}

Server(config, null, initialize)
