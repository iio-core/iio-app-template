const IIO_SERVER_PORT =
  process.env.NODE_ENV === 'production' ? 8080
    : (process.env.IIO_SERVER_PORT ? parseInt(process.env.IIO_SERVER_PORT) : 4093)

const redisCfg = require('./redis')

module.exports = {
  settings: {
    rpcTimeout: 10000,
    _unified: true
  },
  options: {
    apigateway: {
      namespace: process.env.IIOS_NAMESPACE || 'iioat',
      redis: {
        host: redisCfg.REDIS_HOST,
        port: redisCfg.REDIS_PORT,
        db: redisCfg.REDIS_DB
      }
    }
  }
}
