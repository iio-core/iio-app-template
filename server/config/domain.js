const path = require('path')
const fs = require('fs')

let schemas = {}

try {
  let cwd = process.cwd()
  let schemaDir = path.join(cwd, 'dist/data/schemas')
  let schemaFiles = fs.readdirSync(schemaDir)

  for (let file of schemaFiles) {
    let fullpath = path.join(schemaDir, file)
    schemas[path.basename(file, '.json')] =
      JSON.parse(fs.readFileSync(fullpath, 'utf8'))
  }
} catch (err) {
  console.log('WARNING: schemas not available'/* , err */)
}

module.exports = {
  schemas: schemas,
  _unified: true
}
