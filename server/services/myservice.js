const Service = require('@ignitial/iio-app/server/services/core').Service

class MyService extends Service {
  constructor(io, options) {
    super(io, {
      name: 'myservice',
      ...options
    })

    this._register()
  }

  health(args) {
    return new Promise( (resolve, reject) => {
      resolve({ status: 'ok' })
    })
  }
}

module.exports = MyService
