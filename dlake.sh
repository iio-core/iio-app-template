#!/bin/sh

docker stop dlake
docker rm dlake

docker run -d --name dlake \
  -p 20991:20991 \
  -e DLAKE_NAME=dlake \
  -e REDIS_HOST=redis \
  -e MONGODB_URI=mongodb://mongo:27017 \
  -e MONGODB_DBNAME=iioat \
  -e IIOS_NAMESPACE=iioat \
  -e IIOS_SERVER_HOST=0.0.0.0 \
  -e IIOS_SERVER_PORT=20991 \
  -v $PWD/datum:/opt/dlake/datum \
  --link mongo:mongo \
  --link redis:redis \
  ignitial/dlake
